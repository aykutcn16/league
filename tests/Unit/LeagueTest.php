<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;

class LeagueTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_canAddTeams()
    {

        $league = new \App\League;

        $league->add('chelsea');
        $league->add('arsenal');

        $this->assertCount(2, $league->teams);
    }


    public function test_failsWhenTeamsCountIsOdd()
    {
        $this->expectException(\Exception::class);
        $league = new \App\League;

        $league->add('chelsea');
        $league->add('arsenal');
        $league->add('liverpool');
        $league->generateFixture($league->teams);
    }

    public function test_calculatesWeekCountTrue()
    {

        $league = new \App\League;

        $league->add('chelsea');
        $league->add('arsenal');
        $league->add('liverpool');
        $league->add('manchester city');
        $league->generateFixture($league->teams);
        $this->assertEquals(6, $league->getWeekCount());
    }

    public function test_calculatesMatchCountTrue()
    {

        $league = new \App\League;

        $league->add('chelsea');
        $league->add('arsenal');
        $league->add('liverpool');
        $league->add('manchester city');
        $league->add('Leicester City');
        $league->add('Everton');
        $league->generateFixture($league->teams);
        $this->assertEquals(30, $league->getMatchCount());
    }


}
