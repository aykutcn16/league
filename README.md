League Simulation

This simulation allows you to simulate 4 team league and play matches week by week.

-[x] Model Creation
-[x] League installation
-[x] Simulation
-[x] Play
-[x] Play All
-[x] Edit
-[ ] Prediction (Half working sample. I had problems with calculation of probability)

I wrote a class named League. All operations are running from that class.
AppController is web entrypoint for some of that operations.

For front i use jquery & Axios. it lacks styling. i dont use any other library such as 
jquery templates etc.

