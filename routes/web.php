<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', ['as' => 'index', 'uses' => 'App\Http\Controllers\AppController@index']);
Route::get('/start', ['as' => 'start', 'uses' => 'App\Http\Controllers\AppController@start']);
Route::get('/data', ['as' => 'data', 'uses' => 'App\Http\Controllers\AppController@data']);
Route::get('/play/{week}', ['as' => 'index', 'uses' => 'App\Http\Controllers\AppController@play']);
Route::get('/playall', ['as' => 'index', 'uses' => 'App\Http\Controllers\AppController@playAll']);
Route::get('/edit', ['as' => 'index', 'uses' => 'App\Http\Controllers\AppController@edit']);
