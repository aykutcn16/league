<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Premier League</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap"
          rel="stylesheet">

    <link rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
          crossorigin="anonymous">

    <style>
        body {
            font-family: 'Nunito';
        }
    </style>
</head>
<body>

<table style="border: 1px solid gray; width: 50%; margin: 0 auto">
    <tr>
        <td colspan="2"><h1 class="title"></h1></td>
    </tr>

    <tr>
        <td style="width: 40%;">League Table</td>
        <td style="width: 30%;">Match Results</td>
        <td style="width: 30%;" class="prediction">Prediction</td>
    </tr>

    <tr>
        <td>
            <table class="table table-striped" id="pointTable" style="width: 100%;">
                <thead>
                <tr>
                    <td>Teams</td>
                    <td>PTS</td>
                    <td>P</td>
                    <td>W</td>
                    <td>D</td>
                    <td>L</td>
                    <td>GD</td>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </td>
        <td>
            <table class="table table-striped" id="matchTable" style="width: 100%;">
                <thead>
                <tr>
                    <td colspan="4"><span>Match Results</span></td>
                </tr>
                <tbody>

                </tbody>
            </table>
        </td>
        <td class="table table-striped" class="prediction">
            <table id="predictionTable" style="width: 100%;">
                <thead>
                <tr>
                    <td>Teams</td>
                    <td>Chance</td>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <button id="playAll">Play All</button>
            <button id="startNew">Start new</button>
        </td>
        <td>
            <button id="nextWeek">NextWeek</button>
        </td>
    </tr>

</table>


<script src="https://code.jquery.com/jquery-3.3.1.min.js"

        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"

        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"

        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.0/axios.min.js"

        crossorigin="anonymous"></script>
<script type="text/javascript">

    function nth(n) {
        return ["st", "nd", "rd"][((n + 90) % 100 - 10) % 10 - 1] || "th"
    }

    var week = 1;

    function refreshData(url) {
        axios.get(url).then(function (response) {
            var $data = response.data;

            var template = "";
            $.each($data.table, function (index, value) {
                template += "<tr>";
                template += "<td>" + value.name + '</td>';
                template += "<td>" + value.point + '</td>';
                template += "<td>" + value.played + '</td>';
                template += "<td>" + value.won + '</td>';
                template += "<td>" + value.draw + '</td>';
                template += "<td>" + value.lost + '</td>';
                template += "<td>" + (value.goalsfor - value.goalsagainst) + '</td>';

                template += "</tr>";
            });
            $('#pointTable tbody').html(template);

            template = "";
            $.each($data.matchData, function (index, value) {
                template += "<tr>";
                template += "<td>" + value.home_team + '</td>';
                template += "<td>" + value.home_goals + '</td>';
                template += "<td>" + value.away_goals + '</td>';
                template += "<td>" + value.away_team + '</td>';

                template += "</tr>";
            });


            $('#matchTable tbody').html(template);

            template = "";
            if ($data.prediction) {
                $.each($data.prediction || [], function (index, value) {
                    template += "<tr>";
                    template += "<td>" + value.name + '</td>';
                    template += "<td>" + value.chance + '</td>';
                    template += "</tr>";
                });

                $('#predictionTable tbody').html(template);
                $('.prediction').show();
            } else {
                $('#predictionTable tbody').html('');
                $('.prediction').hide();
            }

            week = $data.week;
            $('.title').html(week + nth(week) + ' week results');

        })
    }

    $(document).ready(function () {
        refreshData('/data');
    });

    $('#nextWeek').click(function (event) {
        event.preventDefault();

        refreshData('/play/' + (week + 1));

    });

    $('#startNew').click(function (event) {
        event.preventDefault();

        refreshData('/start');

    })

    $('#playAll').click(function (event) {
        event.preventDefault();

        refreshData('/playall');

    })
</script>


</body>
</html>


