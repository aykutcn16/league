<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Match
 *
 * @property int $id
 * @property int $week
 * @property int $home_team
 * @property int $away_team
 * @property int|null $home_goals
 * @property int|null $away_goals
 * @property int|null $winner
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Match newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Match newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Match query()
 * @method static \Illuminate\Database\Eloquent\Builder|Match whereAwayGoals($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Match whereAwayTeam($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Match whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Match whereHomeGoals($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Match whereHomeTeam($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Match whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Match whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Match whereWeek($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Match whereWinner($value)
 * @mixin \Eloquent
 */
class Match extends Model
{
    use HasFactory;

    protected $table = 'matches';

    protected $fillable = [
        'week',
        'home_team',
        'away_team',
        'home_goals',
        'away_goals',
        'winner',
        'completed',
        'created_at',
        'updated_at',
    ];
}
