<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\League;

class AppController extends Controller
{
    //Serve html page
    public function index(Request $request)
    {
        return response()->view('index');

    }

    //Get Data for initial rendering
    public function data(Request $request)
    {
        $leagueClass = new League();
        return response()->json($leagueClass->table());

    }

    //Starts new league with determined Teams
    public function start(Request $request)
    {
        $leagueClass = new League();

        $leagueClass->add('Chelsea');
        $leagueClass->add('Liverpool');
        $leagueClass->add('Manchester City');
        $leagueClass->add('Arsenal');
        return response()->json($leagueClass->setupLeague());

    }

    //Simulate specified week.
    public function play(Request $request, $week)
    {
        $leagueClass = new League();;
        return response()->json($leagueClass->playWeek($week));

    }

    //Simulate all weeks.
    public function playAll(Request $request)
    {
        $leagueClass = new League();
        return response()->json($leagueClass->playAll());

    }

    //Edit endpoint for
    public function edit(Request $request)
    {
        list($matchId,$homeGoals,$awayGoals) = $request->only(['matchId, homeGoals, awayGoals']);
        $leagueClass = new League();
        $leagueClass->editMatch($matchId,$homeGoals, $awayGoals);
        return response()->json($leagueClass->table());

    }

}

;
