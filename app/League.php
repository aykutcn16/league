<?php

namespace App;

use App\Models\Match;
use App\Models\Team;
use Illuminate\Support\Facades\DB;
use function PHPUnit\Framework\matches;

class League
{
    public $teams = [];
    public $matchTable = [];

    public function add(string $teamName)
    {
        $this->teams[] = $teamName;
    }

    public function matchupTeams($teams)
    {
        //Round Robin with circle method

        //Needs to be even
        $teamCount = count($teams);
        if ($teamCount % 2 != 0) {
            return false;
        }

        $half = $teamCount / 2;

        $weeks = [];

        for ($r = 0; $r < $teamCount - 1; $r++) {
            for ($i = 0; $i < $half; $i++) {
                $weeks[$r][] = [$teams[$i], $teams[$teamCount - 1 - $i]];
            }
            // RoundRobin Shift. Except first
            $teams[] = array_splice($teams, 1, 1)[0];
        }
        // Change teams
        $teams[] = array_splice($teams, 1, 1)[0];

        return $weeks;

    }

    public function generateFixture($teams)
    {
        //There is two matchup, because from my understanding
        //A Football league is consists of two halfs and each teams confronts other teams twice
        //One home game and one away game
        $fixture = $this->matchupTeams($teams);

        //Global Fixture variable
        $matchTable = [];


        //can fixture valid ?
        if ($fixture) {
            //Add home or away for each match for first half
            //First home is zero
            foreach ($fixture as $index => $week) {
                $matchTable[$index + 1] = array_map(function ($match) {
                    $match['home'] = 0;
                    return $match;
                }, $week);
            }

            //Append Second half as away
            foreach ($fixture as $index => $week) {
                $matchTable[$index + 1 + count($fixture)] = array_map(function ($match) {
                    $match['home'] = 1;
                    return $match;
                }, $week);
            }
        } else {
            throw new \Exception('Teams count is not even it seems');
        }

        $this->matchTable = $matchTable;

    }

    public function getFixture()
    {
        return $this->matchTable;
    }

    public function getWeekCount()
    {
        return count($this->matchTable);
    }

    public function getMatchCount()
    {
        return count($this->matchTable) * (count($this->teams) / 2);
    }

    public function setupLeague()
    {
        $teams = $this->teams;


        if (count($teams) < 2 || count($teams) % 2 != 0) {
            throw new \Exception('There is no teams or wrong number of teams');
        }

        Match::truncate();
        Team::truncate();

        //Create Teams fresh database;
        $teamData = array_map(function ($team) {
            return ['name' => $team];
        }, $teams);
        Team::insert($teamData);

        //TeamIds
        $teamIds = Team::all()->pluck('id')->toArray();

        //Generate Fixture
        $this->generateFixture($teamIds);

        $fixture = $this->matchTable;

        $matches = [];
        foreach ($fixture as $weekId => $week) {
            foreach ($week as $match) {
                $matchData = [
                    'week' => $weekId,
                    'home_team' => $match['home'] == 0 ? $match[0] : $match[1],
                    'away_team' => $match['home'] == 0 ? $match[1] : $match[0],
                ];
                $matches[] = $matchData;
            }
        }

        Match::insert($matches);
        $this->playWeek(1);
        return $this->table();
    }

    public function loadLeague()
    {

        $teamIds = Team::all()->pluck('id')->toArray();

        //Generate Fixture
        $this->generateFixture($teamIds);
    }

    public function playWeek($week = 1, $result = true)
    {
        $this->loadLeague();

        $matchesOfWeek = Match::whereWeek($week)->whereCompleted(false)->get();

        $matchesOfWeek->each(function ($match) {

            //Extended winning logic
            //For now its random
            //Play fair
            $homeTeamScores = rand(0, 10);
            $awayTeamScores = rand(0, 10);

            //individual increment columns not existing so in one query we update multiple
            //increment

            $homeTeamData = [];
            $awayTeamData = [];
            $winner = null;

            if ($homeTeamScores > $awayTeamScores) {
                $homeTeamData['won'] = DB::raw('won + 1');
                $homeTeamData['point'] = DB::raw('point + 3');
                $awayTeamData['lost'] = DB::raw('lost + 1');
                $winner = $match->home_team;
            } else if ($homeTeamScores < $awayTeamScores) {
                $awayTeamData['won'] = DB::raw('won + 1');
                $awayTeamData['point'] = DB::raw('point + 3');
                $homeTeamData['lost'] = DB::raw('lost + 1');
                $winner = $match->away_team;
            } else {
                $homeTeamData['draw'] = DB::raw('draw + 1');
                $awayTeamData['draw'] = DB::raw('draw + 1');
                $homeTeamData['point'] = DB::raw('point + 1');
                $awayTeamData['point'] = DB::raw('point + 1');
            }

            $homeTeamData['goalsfor'] = DB::raw('goalsfor + ' . $homeTeamScores);
            $homeTeamData['goalsagainst'] = DB::raw('goalsagainst + ' . $awayTeamScores);
            $homeTeamData['played'] = DB::raw('played + 1');

            $awayTeamData['goalsfor'] = DB::raw('goalsfor + ' . $awayTeamScores);
            $awayTeamData['goalsagainst'] = DB::raw('goalsagainst + ' . $homeTeamScores);
            $awayTeamData['played'] = DB::raw('played + 1');


            Team::whereId($match->home_team)->update($homeTeamData);
            Team::whereId($match->away_team)->update($awayTeamData);

            $matchData = [
                'home_goals' => $homeTeamScores,
                'away_goals' => $awayTeamScores,
                'completed' => true,
                'winner' => $winner
            ];

            $match->update($matchData);


        });

        return $this->table();
    }

    public function prediction($table, $thisWeek, $teamNames)
    {
        $totalWeek = $this->getWeekCount();
        $firstTeam = $table[0];
        //howManyWeeks remaining in season
        $howManyWeeksRemaining = $totalWeek - $thisWeek;

        //Total Percent of championship
        $totalPercent = $totalWeek;
        //Predictions array
        $predictionCalculations = [];

        $eligibleTeams = [];
        $nonEligibleTeams = [];

        //We are starting from lowest team in list
        foreach (array_reverse($table, true) as $key => $team) {
            //it shouldnt be champion
            if ($key != 0) {
                //Has mathematical chance to become champion ?
                if ($firstTeam['point'] - $team['point'] < ($howManyWeeksRemaining * 3)) {
                    //Somehow they have
                    $eligibleTeams[$key] = $team;
                } else {
                    $nonEligibleTeams[$key] = $team;
                }
            }
        }


        $eligibleTeams[0] = $firstTeam;

        foreach ($eligibleTeams as $eligibleTeam) {
            $predictionCalculations[$eligibleTeam['id']] = [
                'name' => $teamNames[$eligibleTeam['id']],
                'chance' => ceil((100 / ($totalWeek * 3)) * $eligibleTeam['point'])
            ];
        }

        foreach ($nonEligibleTeams as $nonEligibleTeam) {
            $predictionCalculations[$nonEligibleTeam['id']] = [
                'name' => $teamNames[$nonEligibleTeam['id']],
                'chance' => 0
            ];
        }

        return $predictionCalculations;
    }

    public function table()
    {
        //IF teams not loaded //load entire league
        if (!$this->teams) {
            $this->loadLeague();
        }

        //Sorted By points first and goaldifference second
        $table = Team::orderBy('point', 'DESC')->orderBy(DB::raw('goalsfor - goalsagainst'), 'DESC')
            ->get()->toArray();

        //Future use id=>teamName
        $teamNames = Team::all()->pluck('name', 'id')->toArray();

        //get Current Week
        $thisWeek = Match::where('completed', true)->max('week');

        //Get this weels matches
        $matches = Match::whereWeek($thisWeek)->get();

        //compile matches data
        $matchData = $matches->map(function ($match) use ($teamNames) {
            return [
                'home_team' => $teamNames[$match->home_team],
                'home_goals' => $match->home_goals,
                'away_team' => $teamNames[$match->away_team],
                'away_goals' => $match->away_goals,
            ];
        });

        $response['table'] = $table;
        $response['matchData'] = $matchData;
        $response['week'] = $thisWeek;
        if ($thisWeek > 3) {
            //Get Predictions
            $response['prediction'] = $this->prediction($table, $thisWeek, $teamNames);
        }

        return $response;
    }

    public function playAll()
    {
        //Load League
        $this->loadLeague();
        //I like foreach so this is for foreach
        $weeks = range(1, $this->getWeekCount());

        foreach ($weeks as $week) {
            //Play every week
            $this->playWeek($week, false);
        }
        //Return data
        return $this->table();
    }

    public function editMatch($matchId, $homeGoals, $awayGoals)
    {
        //Load League
        $this->loadLeague();
        //Get match
        $match = Match::whereId($matchId)->first();

        if ($match) {
            //Data for new results
            $winner = null;
            if ($homeGoals > $awayGoals) {
                $winner = $match->home_team;
            } else if ($homeGoals < $awayGoals) {
                $winner = $match->away_team;
            }
            $matchData = [
                'home_goals' => $homeGoals,
                'away_goals' => $awayGoals,

                'winner' => $winner
            ];

            //Update match
            $match->update($matchData);
            //Recalculate both teams from matches data.
            $this->reCalculateForTeam($match->home_team);
            $this->reCalculateForTeam($match->away_team);


        }

        return false;

    }

    public function reCalculateForTeam($teamId)
    {
        $this->loadLeague();

        $team = Team::whereId($teamId)->first();

        if ($team) {
            $matches = Match::where('home_team', $team->id)
                ->orWhere('away_team', $team->id)->get();

            $teamData = [
                'played' => 0,
                'won' => 0,
                'draw' => 0,
                'lost' => 0,
                'goalsfor' => 0,
                'goalsagainst' => 0,
                'point' => 0,
            ];

            $matches->each(function ($match) use (&$teamData, $team) {
                $teamData['played'] = $teamData['played'] + 1;
                if ($match->winner == $team->id) {
                    $teamData['won'] = $teamData['won'] + 1;
                    $teamData['point'] = $teamData['point'] + 3;
                } elseif ($match->winner != null) {
                    $teamData['lost'] = $teamData['lost'] + 1;
                } else {
                    $teamData['draw'] = $teamData['draw'] + 1;
                    $teamData['point'] = $teamData['point'] + 1;
                }

                $teamData['goalsfor'] = $teamData['goalsfor'] +
                    (($match->home_team == $team->id) ? $match->home_goals : $match->away_goals);

                $teamData['goalsagainst'] = $teamData['goalsagainst'] +
                    (($match->home_team == $team->id) ? $match->away_goals : $match->home_goals);

            });

            $team->update($teamData);


        }

        return false;

    }


}

{
}
